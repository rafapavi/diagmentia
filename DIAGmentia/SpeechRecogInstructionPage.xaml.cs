﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace DIAGmentia
{
    public partial class SpeechRecogInstructionPage : Page
    {
        public SpeechRecogInstructionPage()
        {
            this.InitializeComponent();
        }

        private void btnStart_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SpeechRecogPage));
        }

        private void btnQuit_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
    }
}
