﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using Windows.Graphics.Display;

namespace DIAGmentia
{
    public partial class RehabilitatePage : Page
    {

        public RehabilitatePage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
        }


        private void btnReducedSentences_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(RehabTestExplainationPage), btnReduceSentencesLabel.Text);
        }

        private void btnSubordinatedSentences_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(RehabTestExplainationPage), btnSubordinateSentencesLabel.Text);
        }

        private void btnGoBack_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
    }
}
