﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Display;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using DIAGmentia.SpeechDetectedObjects;

namespace DIAGmentia
{
    public partial class RehabResultPage : Page
    {
        private RehabResult receivedResult;

        private int[] reducedSentences = { 2, 1, 3 };
        private int[] subordinateSentences = { 1, 3, 3 };
        public RehabResultPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            receivedResult  = e.Parameter as RehabResult;
            if (receivedResult.TestType.Equals("Reduced Sentences Test"))
            {
                if (receivedResult.TextDetected.Equals(""))
                {
                    txtDetectedSpeech.Text = "Didn't say anything...";
                }
                else
                {
                    txtDetectedSpeech.Text = receivedResult.TextDetected;
                }
                txtYourResult.Text = "No. of Reduced Sentence in your Description: " + receivedResult.NumberOfReducedSentences.ToString();
                txtNormalResult.Text = "No. of Ideal Reduced Sentences: " + reducedSentences[receivedResult.ImageNo - 1].ToString();
            }
            else if (receivedResult.TestType.Equals("Subordinate Sentences Test"))
            {
                if(receivedResult.TextDetected.Equals(""))
                {
                    txtDetectedSpeech.Text = "Didn't say anything...";
                }
                else
                {
                    txtDetectedSpeech.Text = receivedResult.TextDetected;
                }
                txtYourResult.Text = "No. of Subordinate Sentence in your Description: " + receivedResult.NumberOfReducedSentences.ToString();
                txtNormalResult.Text = "No. of Ideal Subordinate Sentences: " + subordinateSentences[receivedResult.ImageNo - 1].ToString();
            }
        }

        private void btnTestAgain_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Frame.Navigate(typeof(RehabilitatePage));
        }
    }
}
