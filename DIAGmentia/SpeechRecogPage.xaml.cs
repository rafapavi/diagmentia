﻿using System;
using System.Text;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Media.SpeechRecognition;
using Windows.Globalization;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Web;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using Windows.Security.Cryptography.Certificates;
using Windows.Graphics.Display;
using System.Threading;
using Windows.UI.Xaml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml.Media;
using Windows.UI.Popups;

namespace DIAGmentia
{
    public partial class SpeechRecogPage : Page
    {
        private bool listening;
        private SpeechRecognizer speechRecognizer;
        private StringBuilder dictatedTextBuilder;
        private static uint HResultPrivacyStatementDeclined = 0x80045509;

        private HttpBaseProtocolFilter filter;
        private HttpClient httpClient;
        private CancellationTokenSource cts;


        private MessageWebSocket messageWebSocket;
        //private DataWriter messageWriter;
        private const int THRESHOLD_WORD_COUNT = 70;

        public SpeechRecogPage()
        {
            filter = new HttpBaseProtocolFilter();
            httpClient = new HttpClient(filter);
            cts = new CancellationTokenSource();

            dictatedTextBuilder = new StringBuilder();
            listening = false;
            this.InitializeComponent();
        }

        public async static Task<bool> RequestMicrophonePermission()
        {
            try
            {
                MediaCaptureInitializationSettings setting = new MediaCaptureInitializationSettings();
                setting.StreamingCaptureMode = StreamingCaptureMode.Audio;
                setting.MediaCategory = MediaCategory.Speech;
                MediaCapture capture = new MediaCapture();
                await capture.InitializeAsync(setting);
            }
            catch (TypeLoadException)
            {
                var m = new Windows.UI.Popups.MessageDialog("Media Player components are unavailable.");
                await m.ShowAsync();
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -1072845856)
                {
                    var m = new MessageDialog("No audio capture device are present on this system");
                    await m.ShowAsync();
                    return false;
                }
                else
                {
                    throw;
                }
            }
            return true;
        }

        private async Task InitializeRecognizer(Language recognizerLanguage)
        {
            if (speechRecognizer != null)
            {
                speechRecognizer.StateChanged -= SpeechRecognition_StateChanged;
                speechRecognizer.HypothesisGenerated -= SpeechRecognizer_HypothesisGenerated;
                speechRecognizer.ContinuousRecognitionSession.ResultGenerated -= ContinuousRecognitionSession_ResultGenerated;
                speechRecognizer.ContinuousRecognitionSession.Completed -= ContinuousRecognitionSession_Completed;

                this.speechRecognizer.Dispose();
                this.speechRecognizer = null;
            }
            this.speechRecognizer = new SpeechRecognizer(recognizerLanguage);

            speechRecognizer.StateChanged += SpeechRecognition_StateChanged;

            var dictationConstraint = new SpeechRecognitionTopicConstraint(SpeechRecognitionScenario.Dictation, "dictation");
            speechRecognizer.Constraints.Add(dictationConstraint);
            SpeechRecognitionCompilationResult result = await speechRecognizer.CompileConstraintsAsync();
            if (result.Status != SpeechRecognitionResultStatus.Success)
            {
                NotifyUser("Grammar Compilation Failed:" + result.Status.ToString(), "error");
            }
            speechRecognizer.HypothesisGenerated += SpeechRecognizer_HypothesisGenerated;
            speechRecognizer.ContinuousRecognitionSession.ResultGenerated += ContinuousRecognitionSession_ResultGenerated;
            speechRecognizer.ContinuousRecognitionSession.Completed += ContinuousRecognitionSession_Completed;
        }

        private async void SpeechRecognition_StateChanged(SpeechRecognizer sender, SpeechRecognizerStateChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                NotifyUser(args.State.ToString(), "status");
            });
        }

        private async void SpeechRecognizer_HypothesisGenerated(SpeechRecognizer sender, SpeechRecognitionHypothesisGeneratedEventArgs args)
        {
            string hypothesis = args.Hypothesis.Text;
            string textboxContent = dictatedTextBuilder.ToString() + " " + hypothesis + "...";
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                txtResponse.Text = textboxContent;
                System.Diagnostics.Debug.WriteLine(textboxContent, "Text");
                System.Diagnostics.Debug.WriteLine(getWordCount(textboxContent.Trim()), "Length");
                var percentOfWords = (Decimal.Divide(getWordCount(textboxContent.Trim()), THRESHOLD_WORD_COUNT)) * 100;
                System.Diagnostics.Debug.WriteLine(Decimal.Divide(getWordCount(textboxContent.Trim()), THRESHOLD_WORD_COUNT), "Percentage");
                pBarWordCount.Value = double.Parse(percentOfWords.ToString());
            });
        }

        private int getWordCount(string text)
        { 
            int wordCount = 0, index = 0;

            while (index < text.Length)
            {
                // check if current char is part of a word
                while (index < text.Length && Char.IsWhiteSpace(text[index]) == false)
                    index++;

                wordCount++;

                // skip whitespace until next word
                while (index < text.Length && Char.IsWhiteSpace(text[index]) == true)
                    index++;
            }
            return wordCount;
        }
        private async void ContinuousRecognitionSession_ResultGenerated(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            if (args.Result.Confidence == SpeechRecognitionConfidence.High || args.Result.Confidence == SpeechRecognitionConfidence.Medium)
            {
                
                dictatedTextBuilder.Append(args.Result.Text + " ");

                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    txtResponse.Text = dictatedTextBuilder.ToString();
                });
            }
            else
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    txtResponse.Text = dictatedTextBuilder.ToString();
                    String discardedText = args.Result.Text;
                    if (!string.IsNullOrEmpty(discardedText))
                    {
                        discardedText = discardedText.Length <= 25 ? discardedText : (discardedText.Substring(0, 25) + "...");

                        NotifyUser("Please speak louder or nearer to the phone.", "recommend");
                    }
                });
            }
        }

        private async void ContinuousRecognitionSession_Completed(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionCompletedEventArgs args)
        {
            if (args.Status != SpeechRecognitionResultStatus.Success)
            {
                if (args.Status == SpeechRecognitionResultStatus.TimeoutExceeded)
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        NotifyUser("Stopped.Please press start to resume.", "status");
                        txtResponse.Text = dictatedTextBuilder.ToString();
                    });
                }

                else if (args.Status == SpeechRecognitionResultStatus.NetworkFailure)
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        NotifyUser("Please connect to the Internet:" + args.Status.ToString(), "error");
                    });
                }

                else
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        NotifyUser("Completed:" + args.Status.ToString(), "status");
                    });
                }
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    btnStart.Content = "Start";
                    listening = false;
                });
            }

        }
        private async void openPrivacySettings_Click(Hyperlink sender, HyperlinkClickEventArgs args)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settigns:privacy-speechtyping"));
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;   
            bool permissionGained = await RequestMicrophonePermission();
            if (permissionGained)
            {
                btnStart.IsEnabled = true;
                await InitializeRecognizer(SpeechRecognizer.SystemSpeechLanguage);
            }
            else
            {
                this.txtResponse.Text = "Permission to access capture resources was not given by the user, reset the application setting in Settings->Privacy->Microphone.";

            }
        }

        protected async override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (this.speechRecognizer != null)
            {
                if (listening)
                {
                    await this.speechRecognizer.ContinuousRecognitionSession.CancelAsync();
                    listening = false;
                    btnStart.Content = "Start";
                }
                txtResponse.Text = "";

                speechRecognizer.ContinuousRecognitionSession.Completed -= ContinuousRecognitionSession_Completed;
                speechRecognizer.ContinuousRecognitionSession.ResultGenerated -= ContinuousRecognitionSession_ResultGenerated;
                speechRecognizer.HypothesisGenerated -= SpeechRecognizer_HypothesisGenerated;
                speechRecognizer.StateChanged -= SpeechRecognition_StateChanged;

                this.speechRecognizer.Dispose();
                this.speechRecognizer = null;
            }
        }

        public bool TryGetUrl(string uriString, out Uri uri)
        {
            uri = null;

            Uri webSocketUri;
            if (!Uri.TryCreate(uriString.Trim(), UriKind.Absolute, out webSocketUri))
            {
                NotifyUser("Error: Invalid URI", "Error");
                return false;
            }

            if ((webSocketUri.Scheme != "ws") && (webSocketUri.Scheme != "wss"))
            {
                NotifyUser("Error:WebSockets only support ws:// and wss:// schemes.", "error");
                return false;
            }

            uri = webSocketUri;

            return true;

        }

        private void MessageReceived(MessageWebSocket sender, MessageWebSocketMessageReceivedEventArgs args)
        {
            try
            {
                MarshalText(txtResponse, "Message Received;Type: " + args.MessageType + "/r/n");
                using (DataReader reader = args.GetDataReader())
                {
                    reader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;

                    string read = reader.ReadString(reader.UnconsumedBufferLength);
                    MarshalText(txtResponse, read + "\r\n");
                }
            }
            catch (Exception ex)
            {
                WebErrorStatus status = WebSocketError.GetStatus(ex.GetBaseException().HResult);

                if (status == WebErrorStatus.Unknown)
                {
                    throw;
                }

                MarshalText(txtResponse, "Error" + status + "/r/n");
                MarshalText(txtResponse, ex.Message + "/r/n");
            }
        }

        private void MarshalText(TextBlock output, string value)
        {
            MarshalText(output, value, true);
        }

        private void MarshalText(TextBlock output, string value, bool append)
        {
            var ignore = output.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (append)
                {
                    output.Text += value;
                }
                else
                {
                    output.Text = value;
                }
            });
        }

        private void Closed(IWebSocket sender, WebSocketClosedEventArgs args)
        {
            MarshalText(txtResponse, "Closed;Code: " + args.Code + ", Reason: " + args.Reason + "/r/n");
            if (messageWebSocket != null)
            {
                messageWebSocket.Dispose();
                messageWebSocket = null;
            }
        }

        private async void btnEnd_Click(object sender, RoutedEventArgs e)
        {
            btnEnd.IsEnabled = false;
            btnStart.IsEnabled = false;

            if (getWordCount(txtResponse.Text.Trim()) < THRESHOLD_WORD_COUNT)
            {
                var messageDialog = new MessageDialog("You have spoken below the recommended word threshold." + "\n" +
                                                  "This could give you an inaccurate prediction." + "\n" +
                                                  "Are you sure you want to continue?");

                //Successfully Disagrees and goes back to the Main Page
                messageDialog.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(this.PredictionCommandHandler)));
                //Goes back to the Terms and Conditions
                messageDialog.Commands.Add(new UICommand("No", new UICommandInvokedHandler(this.PredictionCommandHandler)));

                messageDialog.DefaultCommandIndex = 0;

                messageDialog.CancelCommandIndex = 1;
                //Display Message box
                await messageDialog.ShowAsync();
            }
            else
            {
                SendResponse();
            }

        }

        public void SendResponse()
        {
            SpeechDetected detectedSpeech = new SpeechDetected();
            detectedSpeech.text = txtResponse.Text.ToString();
            //            //detectedSpeech.text = @"oh little boy's in the cookie jar gram .
            //the girl's standing down and waiting for him to give him her srret some give her some .
            //the mother's doing the dishes .
            //and she's also letting him letting hm .
            //oh the water just fell off over on the floor .
            //uh can I turn the page .
            //okay .
            //that's all there is except she's just getting all wet .
            //no .
            //the little girl was helping the boy to get to the cookie jar .
            //but he started stumbling .
            //he must have fell over because he seems to be falling there .
            //and the mother's over at the sink drying dishes .
            //and it's going on the floor .
            //and three cups three bowls there gram";
            Frame.Navigate(typeof(SpeechDetectedPage), detectedSpeech);

            btnEnd.IsEnabled = true;
            btnStart.IsEnabled = true;
        }

        private void PredictionCommandHandler(IUICommand command)
        {
            //If the user selects "Yes", goes back to the Main Page
            //Else, the message box closes
            if (command.Label.Equals("Yes"))
            {
                SendResponse();
            }
            else
            {
                btnStart.IsEnabled = true;
                btnEnd.IsEnabled = true;
            }
        }
        private async void btnStart_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            btnStart.IsEnabled = false;
            btnEnd.IsEnabled = false;
            if (listening == false)
            {
                if (speechRecognizer.State == SpeechRecognizerState.Idle)
                {
                    btnStart.Content = "Stop";
                    try
                    {
                        listening = true;
                        await speechRecognizer.ContinuousRecognitionSession.StartAsync();
                    }
                    catch (Exception ex)
                    {
                        if ((uint)ex.HResult == HResultPrivacyStatementDeclined)
                        {

                        }
                        else
                        {
                            var m = new Windows.UI.Popups.MessageDialog(ex.Message, "Exception");
                            await m.ShowAsync();
                        }
                        listening = false;
                        btnStart.Content = "Start";
                        btnEnd.IsEnabled = true;
                    }
                    finally
                    {
                        btnStart.IsEnabled = true;
                    }
                }
            }
            else
            {
                listening = false;
                btnEnd.IsEnabled = true;
                btnStart.Content = "Start";
                if (speechRecognizer.State != SpeechRecognizerState.Idle)
                {
                    try
                    {
                        await speechRecognizer.ContinuousRecognitionSession.StopAsync();

                    }
                    catch (Exception ex)
                    {
                        var m = new Windows.UI.Popups.MessageDialog(ex.Message, "Exception");
                        await m.ShowAsync();
                    }
                    finally
                    {
                        btnStart.IsEnabled = true;
                    }
                }
            }

        }

        private void NotifyUser(string strMessage, string type)
        {
            switch (type.ToLower())
            {
                case "status":
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                    break;
                case "error":
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Red);
                    break;
                case "recommend":
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Yellow);
                    break;
                default:
                    break;
            }
            StatusBlock.Text = strMessage;

            if (StatusBlock.Text != String.Empty)
            {
                StatusBlock.Visibility = Visibility.Visible;
                StatusBorder.Visibility = Visibility.Visible;
            }
            else
            {
                StatusBlock.Visibility = Visibility.Collapsed;
                StatusBorder.Visibility = Visibility.Collapsed;
            }
        }

        private void pBarWordCount_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (pBarWordCount.Value >= 70)
            {
                pBarWordCount.Foreground = new SolidColorBrush(Windows.UI.Colors.Green);
            }
        }
        private async void btnQuitTest_Click(object sender, RoutedEventArgs e)
        {
            var messageDialog = new MessageDialog("If you quit now, you will not be able to get a prediction with the current Speech Detection" + "\n" +
                                                  "Are you sure you want to continue?");

            //Successfully Disagrees and goes back to the Main Page
            messageDialog.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(this.CommandInvokedHandler)));
            //Goes back to the Terms and Conditions
            messageDialog.Commands.Add(new UICommand("No", new UICommandInvokedHandler(this.CommandInvokedHandler)));

            messageDialog.DefaultCommandIndex = 0;

            messageDialog.CancelCommandIndex = 1;

            //Display Message box
            await messageDialog.ShowAsync();

            
        }

        private void CommandInvokedHandler(IUICommand command)
        {
            //If the user selects "Yes", goes back to the Main Page
            //Else, the message box closes
            if (command.Label.Equals("Yes"))
            {
                this.Frame.Navigate(typeof(MainPage));
            }
        }
    }
}
