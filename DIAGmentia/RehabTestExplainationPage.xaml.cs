﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Display;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;

namespace DIAGmentia
{
    public partial class RehabTestExplainationPage : Page
    {
        private string TestType;

        public RehabTestExplainationPage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            string data = e.Parameter as string;
            if (data.Equals("Reduced Sentences Test"))
            {
                TestType = data;
                txtTestTitle.Text = data;
                txtTestDescription.Text = "Following the definition set out by de Lira et al. (2011), this feature represents those subordinated sentences without a conjunction but with nominal verb forms(which are either participles or gerund).To obtain the count for this feature, the frequencies of PoS tags(VBG and VBN) are used";
            }
            else if(data.Equals("Subordinate Sentences Test"))
            {
                TestType = data;
                txtTestTitle.Text = data;
                txtTestDescription.Text = "Subordinated sentences are those that are subordinate to the independent primary sentence to which they are linked. Similarly, the number of occurrence for this feature per patient narrative is obtained based on the frequency of the subsentences indicated by the PoS tag(S) detected in the parse tree structure.";
            }
        }

        private void btnTest_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RehabTestPage), TestType);
        }

        private void btnBack_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RehabilitatePage));
        }
    }
}
