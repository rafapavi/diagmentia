﻿using System;
using System.Text;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Media.SpeechRecognition;
using Windows.Globalization;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Web;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using Windows.Security.Cryptography.Certificates;
using Windows.Graphics.Display;
using System.Threading;
using Windows.UI.Xaml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml.Media;
using DIAGmentia.SpeechDetectedObjects;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;

namespace DIAGmentia
{
    public partial class RehabTestPage : Page
    {
        private bool listening;
        private SpeechRecognizer speechRecognizer;
        private StringBuilder dictatedTextBuilder;
        private static uint HResultPrivacyStatementDeclined = 0x80045509;

        private HttpBaseProtocolFilter filter;
        private HttpClient httpClient;
        private CancellationTokenSource cts;


        private MessageWebSocket messageWebSocket;
        //private DataWriter messageWriter;

        private string testType;
        
        // Random image choser
        private int randomImage;
        private Random rng = new Random();

        // Threshold for the min acceptable word count
        private const int THRESHOLD_WORD_COUNT = 120;

        public RehabTestPage()
        {
            filter = new HttpBaseProtocolFilter();
            httpClient = new HttpClient(filter);
            cts = new CancellationTokenSource();

            dictatedTextBuilder = new StringBuilder();
            listening = false;
            this.InitializeComponent();
        }

        public async static Task<bool> RequestMicrophonePermission()
        {
            try
            {
                MediaCaptureInitializationSettings setting = new MediaCaptureInitializationSettings();
                setting.StreamingCaptureMode = StreamingCaptureMode.Audio;
                setting.MediaCategory = MediaCategory.Speech;
                MediaCapture capture = new MediaCapture();
                await capture.InitializeAsync(setting);
            }
            catch (TypeLoadException)
            {
                var m = new Windows.UI.Popups.MessageDialog("Media Player components are unavailable.");
                await m.ShowAsync();
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -1072845856)
                {
                    var m = new Windows.UI.Popups.MessageDialog("No audio capture device are present on this system");
                    await m.ShowAsync();
                    return false;
                }
                else
                {
                    throw;
                }
            }
            return true;
        }

        private async Task InitializeRecognizer(Language recognizerLanguage)
        {
            if (speechRecognizer != null)
            {
                speechRecognizer.StateChanged -= SpeechRecognition_StateChanged;
                speechRecognizer.HypothesisGenerated -= SpeechRecognizer_HypothesisGenerated;
                speechRecognizer.ContinuousRecognitionSession.ResultGenerated -= ContinuousRecognitionSession_ResultGenerated;
                speechRecognizer.ContinuousRecognitionSession.Completed -= ContinuousRecognitionSession_Completed;

                this.speechRecognizer.Dispose();
                this.speechRecognizer = null;
            }
            this.speechRecognizer = new SpeechRecognizer(recognizerLanguage);

            speechRecognizer.StateChanged += SpeechRecognition_StateChanged;

            var dictationConstraint = new SpeechRecognitionTopicConstraint(SpeechRecognitionScenario.Dictation, "dictation");
            speechRecognizer.Constraints.Add(dictationConstraint);
            SpeechRecognitionCompilationResult result = await speechRecognizer.CompileConstraintsAsync();
            if (result.Status != SpeechRecognitionResultStatus.Success)
            {
                NotifyUser("Grammar Compilation Failed:" + result.Status.ToString(), "error");
            }
            speechRecognizer.HypothesisGenerated += SpeechRecognizer_HypothesisGenerated;
            speechRecognizer.ContinuousRecognitionSession.ResultGenerated += ContinuousRecognitionSession_ResultGenerated;
            speechRecognizer.ContinuousRecognitionSession.Completed += ContinuousRecognitionSession_Completed;
        }

        private async void SpeechRecognition_StateChanged(SpeechRecognizer sender, SpeechRecognizerStateChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                NotifyUser(args.State.ToString(), "status");
            });
        }

        private async void SpeechRecognizer_HypothesisGenerated(SpeechRecognizer sender, SpeechRecognitionHypothesisGeneratedEventArgs args)
        {
            string hypothesis = args.Hypothesis.Text;
            string textboxContent = dictatedTextBuilder.ToString() + " " + hypothesis + "...";
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                txtResponse.Text = textboxContent;
                txtWordCount.Text = getWordCount(textboxContent.Trim()).ToString();

            });
        }

        private int getWordCount(string text)
        {
            int wordCount = 0, index = 0;

            while (index < text.Length)
            {
                // check if current char is part of a word
                while (index < text.Length && Char.IsWhiteSpace(text[index]) == false)
                    index++;

                wordCount++;

                // skip whitespace until next word
                while (index < text.Length && Char.IsWhiteSpace(text[index]) == true)
                    index++;
            }
            return wordCount;
        }
        private async void ContinuousRecognitionSession_ResultGenerated(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            if (args.Result.Confidence == SpeechRecognitionConfidence.High || args.Result.Confidence == SpeechRecognitionConfidence.Medium)
            {

                dictatedTextBuilder.Append(args.Result.Text + " ");

                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    txtResponse.Text = dictatedTextBuilder.ToString();
                });
            }
            else
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    txtResponse.Text = dictatedTextBuilder.ToString();
                    String discardedText = args.Result.Text;
                    if (!string.IsNullOrEmpty(discardedText))
                    {
                        discardedText = discardedText.Length <= 25 ? discardedText : (discardedText.Substring(0, 25) + "...");

                        NotifyUser("Please speak louder or nearer to the phone.", "recommend");
                    }
                });
            }
        }

        private async void ContinuousRecognitionSession_Completed(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionCompletedEventArgs args)
        {
            if (args.Status != SpeechRecognitionResultStatus.Success)
            {
                if (args.Status == SpeechRecognitionResultStatus.TimeoutExceeded)
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        NotifyUser("Stopped.Please press start to resume.", "status");
                        txtResponse.Text = dictatedTextBuilder.ToString();
                    });
                }

                else if (args.Status == SpeechRecognitionResultStatus.NetworkFailure)
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        NotifyUser("Please connect to the Internet:" + args.Status.ToString(), "error");
                    });
                }

                else
                {
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        NotifyUser("Completed:" + args.Status.ToString(), "status");
                    });
                }
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    btnStart.Content = "Start";
                    listening = false;
                });
            }

        }
        private async void openPrivacySettings_Click(Hyperlink sender, HyperlinkClickEventArgs args)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settigns:privacy-speechtyping"));
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            randomImage = rng.Next(1, 4);
            if (randomImage == 1)
            {
                imgCookieTheft.Source = new BitmapImage(new Uri("ms-appx:///Assets/cookie_girl.png", UriKind.RelativeOrAbsolute));
                txtInstructions.Text = "Describe the image in at least 15 words";
            }
            else if(randomImage == 2)
            {
                imgCookieTheft.Source = new BitmapImage(new Uri("ms-appx:///Assets/cookie_boy.png", UriKind.RelativeOrAbsolute));
                txtInstructions.Text = "Describe the image in at least 20 words";
            }
            else if (randomImage == 3)
            {
                imgCookieTheft.Source = new BitmapImage(new Uri("ms-appx:///Assets/mother_and_sink.png", UriKind.RelativeOrAbsolute));
                txtInstructions.Text = "Describe the image in at least 22 words";
            }
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            string data = e.Parameter as string;
            testType = data;
            bool permissionGained = await RequestMicrophonePermission();
            if (permissionGained)
            {
                btnStart.IsEnabled = true;
                await InitializeRecognizer(SpeechRecognizer.SystemSpeechLanguage);
            }
            else
            {
                this.txtResponse.Text = "Permission to access capture resources was not given by the user, reset the application setting in Settings->Privacy->Microphone.";

            }
        }

        protected async override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (this.speechRecognizer != null)
            {
                if (listening)
                {
                    await this.speechRecognizer.ContinuousRecognitionSession.CancelAsync();
                    listening = false;
                    btnStart.Content = "Start";
                }
                txtResponse.Text = "";

                speechRecognizer.ContinuousRecognitionSession.Completed -= ContinuousRecognitionSession_Completed;
                speechRecognizer.ContinuousRecognitionSession.ResultGenerated -= ContinuousRecognitionSession_ResultGenerated;
                speechRecognizer.HypothesisGenerated -= SpeechRecognizer_HypothesisGenerated;
                speechRecognizer.StateChanged -= SpeechRecognition_StateChanged;

                this.speechRecognizer.Dispose();
                this.speechRecognizer = null;
            }
        }

        public bool TryGetUrl(string uriString, out Uri uri)
        {
            uri = null;

            Uri webSocketUri;
            if (!Uri.TryCreate(uriString.Trim(), UriKind.Absolute, out webSocketUri))
            {
                NotifyUser("Error: Invalid URI", "Error");
                return false;
            }

            if ((webSocketUri.Scheme != "ws") && (webSocketUri.Scheme != "wss"))
            {
                NotifyUser("Error:WebSockets only support ws:// and wss:// schemes.", "error");
                return false;
            }

            uri = webSocketUri;

            return true;

        }

        private void MessageReceived(MessageWebSocket sender, MessageWebSocketMessageReceivedEventArgs args)
        {
            try
            {
                MarshalText(txtResponse, "Message Received;Type: " + args.MessageType + "/r/n");
                using (DataReader reader = args.GetDataReader())
                {
                    reader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;

                    string read = reader.ReadString(reader.UnconsumedBufferLength);
                    MarshalText(txtResponse, read + "\r\n");
                }
            }
            catch (Exception ex)
            {
                WebErrorStatus status = WebSocketError.GetStatus(ex.GetBaseException().HResult);

                if (status == WebErrorStatus.Unknown)
                {
                    throw;
                }

                MarshalText(txtResponse, "Error" + status + "/r/n");
                MarshalText(txtResponse, ex.Message + "/r/n");
            }
        }

        private void MarshalText(TextBlock output, string value)
        {
            MarshalText(output, value, true);
        }

        private void MarshalText(TextBlock output, string value, bool append)
        {
            var ignore = output.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (append)
                {
                    output.Text += value;
                }
                else
                {
                    output.Text = value;
                }
            });
        }

        private void Closed(IWebSocket sender, WebSocketClosedEventArgs args)
        {
            MarshalText(txtResponse, "Closed;Code: " + args.Code + ", Reason: " + args.Reason + "/r/n");
            if (messageWebSocket != null)
            {
                messageWebSocket.Dispose();
                messageWebSocket = null;
            }
        }

        private async void btnEnd_Click(object sender, RoutedEventArgs e)
        {
            btnEnd.IsEnabled = false;
            btnStart.IsEnabled = false;

            //Message box for disagreeing with the Terms and Conditions.
            if (getWordCount(txtResponse.Text.Trim()) < THRESHOLD_WORD_COUNT)
            {
                var messageDialog = new MessageDialog("You have spoken below the recommended word threshold." + "\n" +
                                                  "This could give you an inaccurate prediction." + "\n" +
                                                  "Are you sure you want to continue?");

                //Successfully Disagrees and goes back to the Main Page
                messageDialog.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(this.CommandInvokedHandler)));
                //Goes back to the Terms and Conditions
                messageDialog.Commands.Add(new UICommand("No", new UICommandInvokedHandler(this.CommandInvokedHandler)));

                messageDialog.DefaultCommandIndex = 0;

                messageDialog.CancelCommandIndex = 1;
                //Display Message box
                await messageDialog.ShowAsync();
            }
            else
            {
                SendResponse();
            }
        }

        public async void SendResponse()
        {
            LoadingBar.IsEnabled = true;
            LoadingBar.Visibility = Visibility.Visible;

            Uri resourceUri = new Uri("http://diagmentia-django.azurewebsites.net/");

            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.Untrusted);
            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.InvalidName);

            httpClient.DefaultRequestHeaders.Accept.TryParseAdd("application/json");
            SpeechDetected sendingResult = new SpeechDetected();
            sendingResult.text = txtResponse.Text.ToString();

            HttpResponseMessage response = await httpClient.PostAsync(resourceUri, new HttpStringContent(JsonConvert.SerializeObject(sendingResult))).AsTask(cts.Token);

            string responseBodyAsText;
            //txtResponse.Text += SerializeHeaders(response);
            responseBodyAsText = await response.Content.ReadAsStringAsync().AsTask(cts.Token);

            cts.Token.ThrowIfCancellationRequested();

            //LoadingBar.IsEnabled = false;
            //LoadingBar.Visibility = Visibility.Collapsed;
            JObject obj = JsonConvert.DeserializeObject(responseBodyAsText) as JObject;
            RehabResult returnResult = new RehabResult();
            returnResult.Class = obj.GetValue("Class").ToString();
            returnResult.AvgPredicate = double.Parse(obj.GetValue("predicate-avg").ToString());
            returnResult.NumberOfCoordinate = int.Parse(obj.GetValue("coordinate").ToString());
            returnResult.Probability = double.Parse(obj.GetValue("Probability").ToString());
            returnResult.NumberOfPredicate = int.Parse(obj.GetValue("predicate-no").ToString());
            returnResult.NumberOfReducedSentences = int.Parse(obj.GetValue("reduced").ToString());
            returnResult.NumberOfSubordinateSentences = int.Parse(obj.GetValue("subordinate").ToString());
            returnResult.TestType = testType;
            returnResult.TextDetected = sendingResult.text;
            returnResult.ImageNo = randomImage;
            Frame.Navigate(typeof(RehabResultPage), returnResult);


            LoadingBar.IsEnabled = false;
            LoadingBar.Visibility = Visibility.Collapsed;

            btnStart.IsEnabled = true;
            btnEnd.IsEnabled = true;
        }
        private void CommandInvokedHandler(IUICommand command)
        {
            //If the user selects "Yes", goes back to the Main Page
            //Else, the message box closes
            if (command.Label.Equals("Yes"))
            {
                SendResponse();
            }
            else
            {
                btnStart.IsEnabled = true;
                btnEnd.IsEnabled = true;
            }
        }

        private async void btnStart_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            btnStart.IsEnabled = false;
            btnEnd.IsEnabled = false;
            if (listening == false)
            {
                if (speechRecognizer.State == SpeechRecognizerState.Idle)
                {
                    btnStart.Content = "Stop";
                    try
                    {
                        listening = true;
                        await speechRecognizer.ContinuousRecognitionSession.StartAsync();
                    }
                    catch (Exception ex)
                    {
                        if ((uint)ex.HResult == HResultPrivacyStatementDeclined)
                        {

                        }
                        else
                        {
                            var m = new Windows.UI.Popups.MessageDialog(ex.Message, "Exception");
                            await m.ShowAsync();
                        }
                        listening = false;
                        btnStart.Content = "Start";
                        btnEnd.IsEnabled = true;
                    }
                    finally
                    {
                        btnStart.IsEnabled = true;
                    }
                }
            }
            else
            {
                listening = false;
                btnEnd.IsEnabled = true;
                btnStart.Content = "Start";
                if (speechRecognizer.State != SpeechRecognizerState.Idle)
                {
                    try
                    {
                        await speechRecognizer.ContinuousRecognitionSession.StopAsync();

                    }
                    catch (Exception ex)
                    {
                        var m = new Windows.UI.Popups.MessageDialog(ex.Message, "Exception");
                        await m.ShowAsync();
                    }
                    finally
                    {
                        btnStart.IsEnabled = true;
                    }
                }
            }

        }

        private void NotifyUser(string strMessage, string type)
        {
            switch (type.ToLower())
            {
                case "status":
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                    break;
                case "error":
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Red);
                    break;
                case "recommend":
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Yellow);
                    break;
                default:
                    break;
            }
            StatusBlock.Text = strMessage;

            if (StatusBlock.Text != String.Empty)
            {
                StatusBlock.Visibility = Visibility.Visible;
                StatusBorder.Visibility = Visibility.Visible;
            }
            else
            {
                StatusBlock.Visibility = Visibility.Collapsed;
                StatusBorder.Visibility = Visibility.Collapsed;
            }
        }

        private void btnQuitTest_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(RehabTestExplainationPage), testType);
        }
    }
}
