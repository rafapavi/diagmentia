﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAGmentia
{
    public class Result
    {
        public string Class { get; set; }
        public double Probability { get; set; }
        public double AvgPredicate { get; set; }
        public int  NumberOfCoordinate { get; set; }
        public int NumberOfPredicate { get; set; }
        public int NumberOfReducedSentences { get; set; }
        public int NumberOfSubordinateSentences { get; set; }
        
    }
}
