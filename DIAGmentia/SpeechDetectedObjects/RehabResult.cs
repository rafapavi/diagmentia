﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAGmentia.SpeechDetectedObjects
{
    public class RehabResult : Result 
    {
        public string TestType { get; set; }
        public string TextDetected { get; set; }
        public int ImageNo { get; set; }
    }
}
