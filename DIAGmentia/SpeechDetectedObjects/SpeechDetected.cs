﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIAGmentia
{
    class SpeechDetected
    {
        public string text { get; set; }

        public override string ToString()
        {
            return this.text;
        }
    }
}
