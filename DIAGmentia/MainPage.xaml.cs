﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Graphics.Display;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DIAGmentia
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>  
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnCookieTheftTest_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AgreementPage));
        }

        public void btnInfoPage_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(InformationPage));
        }

        public void btnRehabilitate_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RehabilitatePage));
        }

        public void btnProgression_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(LineChrt));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            base.OnNavigatedTo(e);
        }

        private void pageRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Width <= 768)
            {
                VisualStateManager.GoToState(this, "Below768Layout", true);
            }
            else if (e.NewSize.Width <= 1130)
            {
                VisualStateManager.GoToState(this, "Below1130Layout", true);
            }
            else
            {
                VisualStateManager.GoToState(this, "DefaultLayout", true);
            }
        }

    }
}
