﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using Windows.Security.Cryptography.Certificates;
using System.Threading;
using Windows.UI.Xaml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml.Navigation;
using Windows.Graphics.Display;
using Windows.UI.Popups;

namespace DIAGmentia
{
    public partial class SpeechDetectedPage : Page
    {
        private HttpBaseProtocolFilter filter;
        private HttpClient httpClient;
        private CancellationTokenSource cts;

        public SpeechDetectedPage()
        {
            filter = new HttpBaseProtocolFilter();
            httpClient = new HttpClient(filter);
            cts = new CancellationTokenSource();

            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            SpeechDetected data = e.Parameter as SpeechDetected;
            txtDetectedSpeech.Text = data.text;
        }

        private async void btnGetPrediction_Click(object sender, RoutedEventArgs e)
        {
            btnGetPrediction.IsEnabled = false;
            btnBackToSpeechDetection.IsEnabled = false;

            LoadingBar.IsEnabled = true;
            LoadingBar.Visibility = Visibility.Visible;
            Uri resourceUri = new Uri("http://diagmentia-django.azurewebsites.net/");

            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.Untrusted);
            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.InvalidName);

            httpClient.DefaultRequestHeaders.Accept.TryParseAdd("application/json");
            SpeechDetected sendingResult = new SpeechDetected();
            sendingResult.text = txtDetectedSpeech.Text.ToString();
            System.Diagnostics.Debug.WriteLine(sendingResult.ToString(), "Sent");

            HttpResponseMessage response = await httpClient.PostAsync(resourceUri, new HttpStringContent(JsonConvert.SerializeObject(sendingResult))).AsTask(cts.Token);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync().AsTask(cts.Token);

            cts.Token.ThrowIfCancellationRequested();

            JObject obj = JsonConvert.DeserializeObject(responseBodyAsText) as JObject;
            System.Diagnostics.Debug.WriteLine(obj, "Return");
            Result returnResult = new Result();
            returnResult.Class = obj.GetValue("Class").ToString();
            returnResult.AvgPredicate = double.Parse(obj.GetValue("predicate-avg").ToString());
            returnResult.NumberOfCoordinate = int.Parse(obj.GetValue("coordinate").ToString());
            returnResult.Probability = double.Parse(obj.GetValue("Probability").ToString());
            returnResult.NumberOfPredicate = int.Parse(obj.GetValue("predicate-no").ToString());
            returnResult.NumberOfReducedSentences = int.Parse(obj.GetValue("reduced").ToString());
            returnResult.NumberOfSubordinateSentences = int.Parse(obj.GetValue("subordinate").ToString());

            LoadingBar.IsEnabled = false;
            LoadingBar.Visibility = Visibility.Collapsed;

            Frame.Navigate(typeof(ResultPage), returnResult);

            btnGetPrediction.IsEnabled = true;
            btnBackToSpeechDetection.IsEnabled = true;
        }

        private async void btnBackToSpeechDetection_Click(object sender, RoutedEventArgs e)
        {
            var messageDialog = new MessageDialog("If you go back, you will have to restart the Speech Detection" + "\n" +
                                                  "Are you sure you want to continue?");

            //Successfully Disagrees and goes back to the Main Page
            messageDialog.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(this.CommandInvokedHandler)));
            //Goes back to the Terms and Conditions
            messageDialog.Commands.Add(new UICommand("No", new UICommandInvokedHandler(this.CommandInvokedHandler)));

            messageDialog.DefaultCommandIndex = 0;

            messageDialog.CancelCommandIndex = 1;

            //Display Message box
            await messageDialog.ShowAsync();

            Frame.Navigate(typeof(SpeechRecogPage));
        }

        private void CommandInvokedHandler(IUICommand command)
        {
            //If the user selects "Yes", goes back to the Main Page
            //Else, the message box closes
            if (command.Label.Equals("Yes"))
            {
                this.Frame.Navigate(typeof(SpeechRecogPage));
            }
        }
    }
}
