﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using Windows.Graphics.Display;

namespace DIAGmentia
{
    public partial class AgreementPage : Page
    {
        public AgreementPage()
        {
            this.InitializeComponent();
        }

        private async void btnDisagree_Click(object sender, RoutedEventArgs e)
        {
            //Message box for disagreeing with the Terms and Conditions.
            var messageDialog = new MessageDialog("If you disagree, you would not be able to test yourself." + "\n" + 
                                                  "Are you sure you want to continue?");

            //Successfully Disagrees and goes back to the Main Page
            messageDialog.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(this.CommandInvokedHandler)));
            //Goes back to the Terms and Conditions
            messageDialog.Commands.Add(new UICommand("No", new UICommandInvokedHandler(this.CommandInvokedHandler)));

            messageDialog.DefaultCommandIndex = 0;

            messageDialog.CancelCommandIndex = 1;

            //Display Message box
            await messageDialog.ShowAsync();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            base.OnNavigatedTo(e);
        }
        private void CommandInvokedHandler(IUICommand command)
        {
            //If the user selects "Yes", goes back to the Main Page
            //Else, the message box closes
            if (command.Label.Equals("Yes"))
            {
                this.Frame.Navigate(typeof(MainPage));
            }
        }
        private void btnAgree_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SpeechRecogInstructionPage));
        }

        /*
        Prevents focus on the text box in the page.
        */
        private void textBox_GotFocus(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ((TextBox)sender).IsTabStop = false;
        }
    }
}
