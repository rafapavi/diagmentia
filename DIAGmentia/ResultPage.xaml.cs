﻿using System;
using System.IO;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Syncfusion.UI.Xaml.Gauges;
using Windows.UI.Xaml.Navigation;
using SQLite.Net.Attributes;
using Windows.Graphics.Display;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409
namespace DIAGmentia
{

    public class Customer
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Date { get; set; }
        public int Per { get; set; }
    }
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ResultPage : Page
    {

        private DispatcherTimer timer = new DispatcherTimer();
        SfCircularGauge circularGauge = new SfCircularGauge();
        public Color uwp_Color;
        SQLite.Net.SQLiteConnection conn;
        Result data;
        Result newResult;


        //private MainPageViewModel vm; 

        public int percentage_differnce;
        public int feature_1_val ;
        public int feature_2_val ;
        public int feature_3_val ;
        public int feature_4_val ;


        private void click_back_btn(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            newResult = e.Parameter as Result;
            //txtResult.Text = data.Class + "\n" + data.Probability
            if (newResult.Class.Equals("AD"))
            {
                percentage_differnce = Convert.ToInt32(newResult.Probability);
            }
            else if (newResult.Class.Equals("Normal"))
            {
                percentage_differnce = 100 - Convert.ToInt32(newResult.Probability);
            }
            //dd-MM-yyyy
            //DateTime thisday = DateTime.Now;
            //Add_Click(thisday.ToString("g"), percentage_differnce);
            feature_1_val = newResult.NumberOfCoordinate;
            feature_2_val = newResult.NumberOfPredicate;
            feature_3_val = newResult.NumberOfReducedSentences;
            feature_4_val = newResult.NumberOfSubordinateSentences;
            setLinearGaugeValue();
            f1.Text += " :" + feature_1_val;
            f2.Text += " :" + feature_2_val;
            f3.Text += " :" + feature_3_val;
            f4.Text += " :" + feature_4_val;

        }

        private void Add_Click(string date, int per)
        {

            var s = conn.Insert(new Customer()
            {
                Date = date,
                Per = per
            });

        }
        public ResultPage()
        {
            this.InitializeComponent();
            string path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<Customer>();

        }

        public void showRadial(string initClass)
        {
            //timer.Interval = TimeSpan.FromSeconds(2); 
            timer.Start();
            int i = 0;
            if (initClass.Equals("AD"))
            {
                myGrapg.TrailBrush = new SolidColorBrush(Windows.UI.Colors.Red);
                myGrapg.Unit = "Dementia";
                
                
            }
            else
            {
                myGrapg.TrailBrush = new SolidColorBrush(Windows.UI.Colors.Green);
                if (percentage_differnce == 0)
                myGrapg.Unit = "Normal";
                
            }

            timer.Tick += (object sender, object e) =>
            {

                if (i == percentage_differnce + 1)
                {
                    timer.Stop();
                    if (percentage_differnce >= 80)
                    {
                        uwp_Color = Convert_Hex_to_UIColor("#e74c3c");
                        //radialgaugecolor.Color = uwp_Color;
                    }
                }
                else
                {
                    myGrapg.Value = i;
                    i++;
                }
            };
        }

        public void setLinearGaugeValue()
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            data = newResult;
            //txtResult.Text = data.Class + "\n" + data.Probability
            System.Diagnostics.Debug.Write(data.Probability, data.Class);
            percentage_differnce = Convert.ToInt32(data.Probability * 100);

            //"dd-MM-yyyy"
            DateTime thisday = DateTime.Now;
            Add_Click(thisday.ToString("g"), percentage_differnce);
            showRadial(data.Class);
            pointer1.Value = feature_1_val;
            pointer2.Value = feature_2_val;
            pointer3.Value = feature_3_val;
            pointer4.Value = feature_4_val;
        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            for (int i = 0; i < Pivot.Items.Count; i++)
            {
                if (i == Pivot.SelectedIndex)
                {
                    PivotItem selectedPivotItem = Pivot.SelectedItem as PivotItem;
                    (selectedPivotItem.Header as TextBlock).Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    PivotItem unselectedPivotItem = Pivot.Items[i] as PivotItem;
                    (unselectedPivotItem.Header as TextBlock).Foreground = new SolidColorBrush(Colors.LightBlue);
                }
            }
        }


        public static Windows.UI.Color Convert_Hex_to_UIColor(string string_HexColor)
        {
            //----------------<  Convert_Hex_to_UIColor() >----------------
            //original source: http://snipplr.com/view/13358/
            //Remove # if present
            if (string_HexColor.IndexOf('#') != -1)
                string_HexColor = string_HexColor.Replace("#", "");

            //< variables >
            int alpha = 255;
            int red = 0;
            int green = 0;
            int blue = 0;
            //</ variables >

            //----< Convert String to int >---
            if (string_HexColor.Length == 6)
            {
                //--< RRGGBB_to_int >--
                //*like: #RRGGBB
                red = int.Parse(string_HexColor.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                green = int.Parse(string_HexColor.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                blue = int.Parse(string_HexColor.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                //--</ RRGGBB_to_int >--
            }
            else if (string_HexColor.Length == 3)
            {
                //--< RGB_to_int >--
                //not really necessary
                //*like #RGB
                red = int.Parse(string_HexColor[0].ToString() + string_HexColor[0].ToString(), System.Globalization.NumberStyles.AllowHexSpecifier);
                green = int.Parse(string_HexColor[1].ToString() + string_HexColor[1].ToString(), System.Globalization.NumberStyles.AllowHexSpecifier);
                blue = int.Parse(string_HexColor[2].ToString() + string_HexColor[2].ToString(), System.Globalization.NumberStyles.AllowHexSpecifier);
                //--</ RGB_to_int >--
            }
            //----</ Convert String to int >---

            //< Return >
            return Windows.UI.Color.FromArgb(Convert.ToByte(alpha), Convert.ToByte(red), Convert.ToByte(green), Convert.ToByte(blue));
            //</ Return >
            //----------------</  Convert_Hex_to_UIColor() >----------------
        }

    }
}
